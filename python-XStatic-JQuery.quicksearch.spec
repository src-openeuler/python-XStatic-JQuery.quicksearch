%global _empty_manifest_terminate_build 0
Name:		python-XStatic-JQuery.quicksearch
Version:	2.0.3.2
Release:	1
Summary:	JQuery.quicksearch 2.0.3 (XStatic packaging standard)
License:	MIT
URL:		http://plugins.jquery.com/jquery.quicksearch/
Source0:	https://files.pythonhosted.org/packages/92/ee/a020b3c08f1bb5e3744cbe2bba77e217f1d02163c9a82a6f6f8d0245e7ed/XStatic-JQuery.quicksearch-2.0.3.2.tar.gz
BuildArch:	noarch


%description
JQuery.quicksearch JavaScript library packaged for setuptools (easy_install)
/ pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package -n python3-XStatic-JQuery-quicksearch
Summary:	JQuery.quicksearch 2.0.3 (XStatic packaging standard)
Provides:	python-XStatic-JQuery-quicksearch
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-XStatic-JQuery-quicksearch
JQuery.quicksearch JavaScript library packaged for setuptools (easy_install)
/ pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%package help
Summary:	Development documents and examples for XStatic-JQuery.quicksearch
Provides:	python3-XStatic-JQuery-quicksearch-doc
%description help
JQuery.quicksearch JavaScript library packaged for setuptools (easy_install)
/ pip.
This package is intended to be used by **any** project that needs these files.
It intentionally does **not** provide any extra code except some metadata
**nor** has any extra requirements. You MAY use some minimal support code from
the XStatic base package, if you like.
You can find more info about the xstatic packaging way in the package `XStatic`.

%prep
%autosetup -n XStatic-JQuery.quicksearch-2.0.3.2

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-XStatic-JQuery-quicksearch -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Sat Jan 30 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
